# OKS
OKD es la versión upstream apoyada por la comunidad de Red Hat OpenShift Container Platform (OCP). OpenShift expande vanilla Kubernetes en una plataforma de aplicaciones diseñada para uso empresarial a escala. A partir del lanzamiento de OpenShift 4, el sistema operativo predeterminado es Red Hat CoreOS, que proporciona una infraestructura inmutable y actualizaciones automáticas. El sistema operativo predeterminado de OKD es Fedora CoreOS que, como OKD, es la versión upstream de Red Hat CoreOS.

# Creando nuestro laboratorio
Para hablar de Openshift tenemos que hablar obligatoriamente de Kubernetes, y los que seguís nuestro blog sabéis que ya fue tratado en nuestro articulo ["Kubernetes (I)"](http://enmilocalfunciona.io/introduccion-a-kubernetes-i/). Ahí vimos como empezar a trabajar con este orquestador de contenedores Docker. Los conceptos son prácticamente los mismos, aunque Openshift añade algunos como:

 * Builds
 * Deployment
 * Consola Web
 * Projects

Existen 3 versiones de Openshift:

 * [Openshift Origin: Openshift de codigo abierto](https://github.com/openshift)
 * [Openshift Online: Openshift Público](https://www.openshift.com/)
 * [Openshift Enterprise: Openshift para instalación en servidores o cloud privado](https://www.openshift.com/products/enterprise)

# Minishift 
Minishift es una herramienta que te ayuda a correr Openshift de manera local ejecutando un clúster OpenShift de un solo nodo dentro de una máquina virtual.
Minishift ha sido sustituido por el comando oc cluster up soportado en Fedora Linux, CentOS y RHEL. 

 - https://docs.okd.io/3.11/minishift/getting-started/quickstart.html#starting-minishift
 
# Preparing to Install Minishift 
## Overview
Siga el procedimiento apropiado para configurar el hipervisor para su sistema operativo particular.
Minishift utiliza libmachine y su arquitectura de plug-in de controladores para proporcionar una forma coherente de gestionar la máquina virtual Minishift.

Minishift se prueba actualmente con la versión 0.10.0 de docker-machine-driver-kvm.
Continúe con las instrucciones para su distribución de Linux después de haber configurado el binario docker-machine-driver-kvm.
Para obtener más información, consulte la documentación de GitHub del controlador KVM de la máquina Docker: https://github.com/dhiltgen/docker-machine-kvm#quick-start-instructions

# Linux
## On Ubuntu
1. Install libvirt and qemu-kvm on your system:
```
$ sudo apt install qemu-kvm libvirt-daemon libvirt-daemon-system
```
2. Add yourself to the libvirt(d) group:
```
$ sudo usermod -a -G libvirt $(whoami)
```
3. Update your current session to apply the group change:
```
$ newgrp libvirt
```
4. As root, install the KVM driver binary and make it executable as follows:
```
# curl -L https://github.com/dhiltgen/docker-machine-kvm/releases/download/v0.10.0/docker-machine-driver-kvm-ubuntu16.04 -o /usr/local/bin/docker-machine-driver-kvm
# chmod +x /usr/local/bin/docker-machine-driver-kvm
```
## Start libvirtd service
1. Check the status of libvirtd:
```
$ systemctl is-active libvirtd
```
2. If libvirtd is not active, start the libvirtd service:
```
$ sudo systemctl start libvirtd
```

# Configure libvirt networking
Algunas distribuciones configuran la red libvirt predeterminada para usted, mientras que en otras esto puede que tenga que hacerlo manualmente.
Verifique el estado de su red:
1. Check your network status:
```
$ sudo virsh net-list --all
```
2. Start the default libvirt network:
```
$ sudo virsh net-start default
```
3. Now mark the default network as autostart:
```
sudo virsh net-autostart default
```

## Solución de problemas
```
$ sudo virsh net-start default
 error: Failed to start network default
 error: internal error: Failed to initialize a valid firewall backend
```
Instalar firewalld, ebtables y reiniciar libvirtd.service:
```
$ sudo apt-get install firewalld
$ sudo  apt-get install ebtables
$ sudo systemctl restart libvirtd.service
```

# Installing Minishift  
1. Download the archive for your operating system from the [Minishift Releases](https://github.com/minishift/minishift/releases) page and extract its contents.
```
$ wget https://github.com/minishift/minishift/releases/download/v1.34.3/minishift-1.34.3-linux-amd64.tgz
```
2. Copy the contents of the directory to your preferred location.
```
$ tar -xzvf minishift-1.34.3-darwin-amd64.tgz
```
3. Add the minishift binary to your PATH environment variable.
```
$ export PATH=$PATH:$HOME/minishift-1.34.3-linux-amd64
```

# Starting Minishift
Run the minishift start command:
```
$ minishift start
```
# To check the IP
```
$ minishift ip
```

# See the Troubleshooting Getting Started topic for information about possible causes and solutions.
- [Minishift startup check failed](https://docs.okd.io/3.11/minishift/troubleshooting/troubleshooting-getting-started.html#minshift-startup-check-failed)

# Minishift comandos
## Minishift Stop
```
$ minishift stop
```
## destruir todas las máquinas virtuales que contienen openshift: 
```
$ minishift delete
```
## Ver registros
```
$ minishift logs
```
## Ejecutar un shell en la máquina virtual que contiene Openshift
```
$ minishift ssh
```

# Primer Inicio de Minishift
## Arquitectura:
 - Notebook: EXO SmartPro Q2 Q5145S
 - CPU Intel i5
 - 8 RAM
 - SSD Western Digital Green 120GB 

## minishift start
```
marcos@linuxmint19:~$ lsb_release -a
No LSB modules are available.
Distributor ID:	LinuxMint
Description:	Linux Mint 19.3 Tricia
Release:	19.3
Codename:	tricia

marcos@linuxmint19:~$ minishift start
-- Starting profile 'minishift'
-- Check if deprecated options are used ... OK
-- Checking if https://github.com is reachable ... OK
-- Checking if requested OpenShift version 'v3.11.0' is valid ... OK
-- Checking if requested OpenShift version 'v3.11.0' is supported ... OK
-- Checking if requested hypervisor 'kvm' is supported on this platform ... OK
-- Checking if KVM driver is installed ... 
   Driver is available at /usr/local/bin/docker-machine-driver-kvm ... 
   Checking driver binary is executable ... OK
-- Checking if Libvirt is installed ... OK
-- Checking if Libvirt default network is present ... OK
-- Checking if Libvirt default network is active ... OK
-- Checking the ISO URL ... OK
-- Downloading OpenShift binary 'oc' version 'v3.11.0'
 53.89 MiB / 53.89 MiB [=====================================================================================================================================] 100.00% 0s-- Downloading OpenShift v3.11.0 checksums ... OK
-- Checking if provided oc flags are supported ... OK
-- Starting the OpenShift cluster using 'kvm' hypervisor ...
-- Minishift VM will be configured with ...
   Memory:    4 GB
   vCPUs :    2
   Disk size: 20 GB

   Downloading ISO 'https://github.com/minishift/minishift-centos-iso/releases/download/v1.17.0/minishift-centos7.iso'
 375.00 MiB / 375.00 MiB [===================================================================================================================================] 100.00% 0s
-- Starting Minishift VM ..................... OK
-- Checking for IP address ... OK
-- Checking for nameservers ... OK
-- Checking if external host is reachable from the Minishift VM ... 
   Pinging 8.8.8.8 ... OK
-- Checking HTTP connectivity from the VM ... 
   Retrieving http://minishift.io/index.html ... OK
-- Checking if persistent storage volume is mounted ... OK
-- Checking available disk space ... 1% used OK
-- Writing current configuration for static assignment of IP address ... WARN
   Importing 'openshift/origin-control-plane:v3.11.0'  CACHE MISS
   Importing 'openshift/origin-docker-registry:v3.11.0'  CACHE MISS
   Importing 'openshift/origin-haproxy-router:v3.11.0'  CACHE MISS
-- OpenShift cluster will be configured with ...
   Version: v3.11.0
-- Pulling the OpenShift Container Image ................................ OK
-- Copying oc binary from the OpenShift container image to VM ... OK
-- Starting OpenShift cluster ...........................................................................
Getting a Docker client ...
Checking if image openshift/origin-control-plane:v3.11.0 is available ...
Pulling image openshift/origin-cli:v3.11.0
E1002 23:20:27.707988    2179 helper.go:173] Reading docker config from /home/docker/.docker/config.json failed: open /home/docker/.docker/config.json: no such file or directory, will attempt to pull image docker.io/openshift/origin-cli:v3.11.0 anonymously
Image pull complete
Pulling image openshift/origin-node:v3.11.0
E1002 23:20:31.434317    2179 helper.go:173] Reading docker config from /home/docker/.docker/config.json failed: open /home/docker/.docker/config.json: no such file or directory, will attempt to pull image docker.io/openshift/origin-node:v3.11.0 anonymously
Pulled 5/6 layers, 84% complete
Pulled 6/6 layers, 100% complete
Extracting
Image pull complete
Checking type of volume mount ...
Determining server IP ...
Using public hostname IP 192.168.42.76 as the host IP
Checking if OpenShift is already running ...
Checking for supported Docker version (=>1.22) ...
Checking if insecured registry is configured properly in Docker ...
Checking if required ports are available ...
Checking if OpenShift client is configured properly ...
Checking if image openshift/origin-control-plane:v3.11.0 is available ...
Starting OpenShift using openshift/origin-control-plane:v3.11.0 ...
I1002 23:21:17.294023    2179 config.go:40] Running "create-master-config"
I1002 23:21:20.569774    2179 config.go:46] Running "create-node-config"
I1002 23:21:21.537212    2179 flags.go:30] Running "create-kubelet-flags"
I1002 23:21:22.361340    2179 run_kubelet.go:49] Running "start-kubelet"
I1002 23:21:22.779783    2179 run_self_hosted.go:181] Waiting for the kube-apiserver to be ready ...
I1002 23:22:26.850986    2179 interface.go:26] Installing "kube-proxy" ...
I1002 23:22:26.853195    2179 interface.go:26] Installing "kube-dns" ...
I1002 23:22:26.853228    2179 interface.go:26] Installing "openshift-service-cert-signer-operator" ...
I1002 23:22:26.853251    2179 interface.go:26] Installing "openshift-apiserver" ...
I1002 23:22:26.853439    2179 apply_template.go:81] Installing "openshift-apiserver"
I1002 23:22:26.854361    2179 apply_template.go:81] Installing "kube-proxy"
I1002 23:22:26.855974    2179 apply_template.go:81] Installing "kube-dns"
I1002 23:22:26.861569    2179 apply_template.go:81] Installing "openshift-service-cert-signer-operator"
I1002 23:22:38.668733    2179 interface.go:41] Finished installing "kube-proxy" "kube-dns" "openshift-service-cert-signer-operator" "openshift-apiserver"
I1002 23:26:03.772742    2179 run_self_hosted.go:242] openshift-apiserver available
I1002 23:26:03.773640    2179 interface.go:26] Installing "openshift-controller-manager" ...
I1002 23:26:03.773671    2179 apply_template.go:81] Installing "openshift-controller-manager"
Adding default OAuthClient redirect URIs ...
I1002 23:26:07.584038    2179 interface.go:41] Finished installing "openshift-controller-manager"
Adding router ...
Adding sample-templates ...
Adding persistent-volumes ...
Adding registry ...
Adding web-console ...
Adding centos-imagestreams ...
I1002 23:26:07.612907    2179 interface.go:26] Installing "openshift-router" ...
I1002 23:26:07.612921    2179 interface.go:26] Installing "sample-templates" ...
I1002 23:26:07.612930    2179 interface.go:26] Installing "persistent-volumes" ...
I1002 23:26:07.612937    2179 interface.go:26] Installing "openshift-image-registry" ...
I1002 23:26:07.612949    2179 interface.go:26] Installing "openshift-web-console-operator" ...
I1002 23:26:07.612958    2179 interface.go:26] Installing "centos-imagestreams" ...
I1002 23:26:07.613037    2179 apply_list.go:67] Installing "centos-imagestreams"
I1002 23:26:07.613866    2179 interface.go:26] Installing "sample-templates/mongodb" ...
I1002 23:26:07.613881    2179 interface.go:26] Installing "sample-templates/postgresql" ...
I1002 23:26:07.613889    2179 interface.go:26] Installing "sample-templates/django quickstart" ...
I1002 23:26:07.613897    2179 interface.go:26] Installing "sample-templates/rails quickstart" ...
I1002 23:26:07.613905    2179 interface.go:26] Installing "sample-templates/sample pipeline" ...
I1002 23:26:07.613913    2179 interface.go:26] Installing "sample-templates/mariadb" ...
I1002 23:26:07.613920    2179 interface.go:26] Installing "sample-templates/mysql" ...
I1002 23:26:07.613933    2179 interface.go:26] Installing "sample-templates/cakephp quickstart" ...
I1002 23:26:07.613942    2179 interface.go:26] Installing "sample-templates/dancer quickstart" ...
I1002 23:26:07.613951    2179 interface.go:26] Installing "sample-templates/nodejs quickstart" ...
I1002 23:26:07.613958    2179 interface.go:26] Installing "sample-templates/jenkins pipeline ephemeral" ...
I1002 23:26:07.614023    2179 apply_list.go:67] Installing "sample-templates/jenkins pipeline ephemeral"
I1002 23:26:07.615622    2179 apply_template.go:81] Installing "openshift-web-console-operator"
I1002 23:26:07.615861    2179 apply_list.go:67] Installing "sample-templates/mongodb"
I1002 23:26:07.616008    2179 apply_list.go:67] Installing "sample-templates/postgresql"
I1002 23:26:07.616147    2179 apply_list.go:67] Installing "sample-templates/django quickstart"
I1002 23:26:07.616283    2179 apply_list.go:67] Installing "sample-templates/rails quickstart"
I1002 23:26:07.616418    2179 apply_list.go:67] Installing "sample-templates/sample pipeline"
I1002 23:26:07.616547    2179 apply_list.go:67] Installing "sample-templates/mariadb"
I1002 23:26:07.616710    2179 apply_list.go:67] Installing "sample-templates/mysql"
I1002 23:26:07.616875    2179 apply_list.go:67] Installing "sample-templates/cakephp quickstart"
I1002 23:26:07.617010    2179 apply_list.go:67] Installing "sample-templates/dancer quickstart"
I1002 23:26:07.617145    2179 apply_list.go:67] Installing "sample-templates/nodejs quickstart"
I1002 23:26:21.467402    2179 interface.go:41] Finished installing "sample-templates/mongodb" "sample-templates/postgresql" "sample-templates/django quickstart" "sample-templates/rails quickstart" "sample-templates/sample pipeline" "sample-templates/mariadb" "sample-templates/mysql" "sample-templates/cakephp quickstart" "sample-templates/dancer quickstart" "sample-templates/nodejs quickstart" "sample-templates/jenkins pipeline ephemeral"
I1002 23:26:55.854935    2179 interface.go:41] Finished installing "openshift-router" "sample-templates" "persistent-volumes" "openshift-image-registry" "openshift-web-console-operator" "centos-imagestreams"
Login to server ...
Creating initial project "myproject" ...
Server Information ...
OpenShift server started.

The server is accessible via web console at:
    https://192.168.42.76:8443/console

You are logged in as:
    User:     developer
    Password: <any value>

To login as administrator:
    oc login -u system:admin


-- Exporting of OpenShift images is occuring in background process with pid 28036.
```

# Deploying a Sample Application
https://docs.okd.io/3.11/minishift/getting-started/quickstart.html#deploy-sample-app

# Links
https://itnext.io/guide-installing-an-okd-4-5-cluster-508a2631cbee
https://www.essiprojects.com/author/jdavidmartinnieto/
https://enmilocalfunciona.io/introduccion-a-openshift-creando-nuestro-laboratorio/
https://docs.okd.io/latest/cli_reference/openshift_cli/getting-started-cli.html
https://docs.okd.io/latest/cli_reference/openshift_cli/developer-cli-commands.html
https://docs.okd.io/latest/cli_reference/openshift_cli/administrator-cli-commands.html
