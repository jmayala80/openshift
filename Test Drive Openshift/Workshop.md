# 1 Your Workshop Environment
First Step: Confirm Your Username!
Look in the box at the top of your screen. Is your username set already? If so it will look like this:

Set User ID above
If your username is properly set, then you can move on. If not, in the above box, enter the user ID you were assigned like this:

Set User ID above
This will customize the links and copy/paste code for this workshop. If you accidently type the wrong username, just click the green recycle icon to reset it.

Throughout this lab you’ll discover how Quarkus can make your development of cloud native apps faster and more productive.

# Click-to-Copy
You will see various code and command blocks throughout these exercises which can be copy/pasted directly by clicking anywhere on the block of text:
```
/* A sample Java snippet that you can copy/paste by clicking */
public class CopyMeDirectly {
    public static void main(String[] args) {
        System.out.println("You can copy this whole class with a click!");
    }
}
```
Simply click once and the whole block is copied to your clipboard, ready to be pasted with CTRL+V (or Command+V on Mac OS).

There are also Linux shell commands that can also be copied and pasted into a Terminal in your Development Environment:

echo "This is a bash shell command that you can copy/paste by clicking"

# The Workshop Environment You Are Using
Your workshop environment consists of several components which have been pre-installed and are ready to use. Depending on which parts of the workshop you’re doing, you will use one or more of:

 - Red Hat OpenShift - You’ll use one or more projects (Kubernetes namespaces) that are your own and are isolated from other workshop students

 - Red Hat CodeReady Workspaces - based on Eclipse Che, it’s a cloud-based, in-browser IDE (similar to IntelliJ IDEA, VSCode, Eclipse IDE). You’ve been provisioned your own personal workspace for use with this workshop. You’ll write, test, and deploy code from here.

 - Red Hat Application Migration Toolkit - You’ll use this to migrate an existing application

 - Red Hat Runtimes - a collection of cloud-native runtimes like Spring Boot, Node.js, and Quarkus

 - Red Hat AMQ Streams - streaming data platform based on Apache Kafka

 - Red Hat SSO - For authentication / authorization - based on Keycloak

Other open source projects like Knative (for serverless apps), Jenkins and Tekton (CI/CD pipelines), Prometheus and Grafana (monitoring apps), and more.

You’ll be provided clickable URLs throughout the workshop to access the services that have been installed for you.

Red Hat offers the fully supported Red Hat Build of Quarkus(RHBQ) with support and maintenance of Quarkus. In this workhop, you will use Quarkus to develop Kubernetes-native microservices and deploy them to OpenShift. Quarkus is one of the runtimes included in Red Hat Runtimes. Learn more about RHBQ.

# How to complete this workshop
Click the "Next >" button at the bottom to advance to the next topic. You can also use the menu on the left to move around the instructions at will.

Good luck, and let’s get started!

# 2 Getting Started with Service Mesh
In this module, you will learn how to prevent cascading failures in a distributed environment, how to detect misbehaving services, and how to avoid having to implement resiliency and monitoring in your business logic. As we transition our applications towards a distributed architecture with microservices deployed across a distributed network, many new challenges await us.

Technologies like containers and container orchestration platforms like OpenShift solve the deployment of our distributed applications quite well, but some challenges remain with distributed apps:

 - Unpredictable failure modes

 - Verifying end-to-end application correctness

 - Unexpected system degradation

 - Continuous topology changes

 - The use of elastic/ephemeral/transient resources

Today, developers are responsible for taking into account these challenges, and do things like:

 - Circuit breaking and Bulkheading (e.g. with Netfix Hystrix)

 - Timeouts/retries

 -  Service discovery (e.g. with Eureka)

 - Client-side load balancing (e.g. with Netfix Ribbon)

Another challenge is each runtime and language addresses these with different libraries and frameworks, and in some cases there may be no implementation of a particular library for your chosen language or runtime.

In this section we’ll explore how to use the OpenShift Service Mesh. Service Mesh is based on the Istio open source project. It enables a much more robust, reliable, and resilient application in the face of the new world of dynamic distributed applications.

# What is Istio?
Logo
[Istio](http://istio.io/) forms the basis for the OpenShift Service Mesh and is an open, platform-independent service mesh designed to manage communications between microservices and applications in a transparent way. It provides behavioral insights and operational control over the service mesh as a whole. It provides a number of key capabilities uniformly across a network of services:

 - Traffic Management - Control the flow of traffic and API calls between services, make calls more reliable, and make the network more robust in the face of adverse conditions.

 - Observability - Gain understanding of the dependencies between services and the nature and flow of traffic between them, providing the ability to quickly identify issues.

 - Policy Enforcement - Apply organizational policy to the interaction between services, ensure access policies are enforced and resources are fairly distributed among consumers. Policy changes are made by configuring the mesh, not by changing application code.

 - Service Identity and Security - Provide services in the mesh with a verifiable identity and provide the ability to protect service traffic as it flows over networks of varying degrees of trustability.

These capabilities greatly decrease the coupling between application code, the underlying platform, and policy. This decreased coupling not only makes services easier to implement, but also makes it simpler for operators to move application deployments between environments or to new policy schemes. Applications become inherently more portable as a result.

Sounds fun, right? Let’s get started!

# Getting Ready for the labs
If you’ve already worked on prior labs today, you’ll be familiar with the CodeReady Workspaces environment and can skip down to the Import Project section below.

You will be using Red Hat CodeReady Workspaces, an online IDE based on [Eclipe Che](https://www.eclipse.org/che/). Changes to files are auto-saved every few seconds, so you don’t need to explicitly save changes.

To get started, [access the CodeReady Workspaces instance](https://codeready-labs-infra.apps.cluster-caba-50d6.caba-50d6.sandbox1286.opentlc.com/) and log in using the username and password you’ve been assigned (e.g. user11/r3dh4t1!):

cdw
Once you log in, you’ll be placed on your personal dashboard. Click on the name of the pre-created workspace on the left, as shown below (the name will be different depending on your assigned number). You can also click on the name of the workspace in the center, and then click on the green user11-namespace that says Open on the top right hand side of the screen.

cdw
After a minute or two, you’ll be placed in the workspace:

cdw
This IDE is based on Eclipse Che (which is in turn based on MicroSoft VS Code editor).

You can see icons on the left for navigating between project explorer, search, version control (e.g. Git), debugging, and other plugins. You’ll use these during the course of this workshop. Feel free to click on them and see what they do:

cdw
If things get weird or your browser appears, you can simply reload the browser tab to refresh the view.

Many features of CodeReady Workspaces are accessed via Commands. You can see a few of the commands listed with links on the home page (e.g. New File.., Git Clone.., and others).

If you ever need to run commands that you don’t see in a menu, you can press F1 to open the command window, or the more traditional Control+SHIFT+P (or Command+SHIFT+P on Mac OS X).

Import project
Let’s import our first project. Click on Git Clone.. (or type F1, enter 'git' and click on the auto-completed Git Clone.. )

cdw
Step through the prompts, using the following value for Repository URL. If you use FireFox, it may end up pasting extra spaces at the end, so just press backspace after pasting:

https://github.com/RedHat-Middleware-Workshops/cloud-native-workshop-v2m3-labs.git
crw
The project is imported into your workspace and is visible in the project explorer after clicking the Explorer pane at the upper right:

crw
IMPORTANT: Check out proper Git branch
To make sure you’re using the right version of the project files, run this command in a CodeReady Terminal:

cd $CHE_PROJECTS_ROOT/cloud-native-workshop-v2m3-labs && git checkout ocp-4.5
Remove other projects
If you’ve completed other modules today (such as cloud-native-workshop-v2m1-labs), remove them from your workspace by right-clicking on the project name in the explorer and choose Delete and accept the warning. Be sure not to delete the new project you just imported for this lab!

remove
The Terminal window in CodeReady Workspaces. You can open a terminal window for any of the containers running in your Developer workspace. For the rest of these labs, anytime you need to run a command in a terminal, you can use the >_ New Terminal command on the right:

codeready-workspace-terminal