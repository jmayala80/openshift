# Openshift Docs
 - https://docs.openshift.com/container-platform/4.1/welcome/index.html

# helm - Helm 3 CLI
Helm 3 es un administrador de paquetes para aplicaciones de Kubernetes que permite definir, instalar y actualizar aplicaciones empaquetadas como Helm Charts.

 - https://mirror.openshift.com/pub/openshift-v4/clients/helm/latest/

# oc - OpenShift Command Line Interface (CLI)
Con la interfaz de línea de comandos de OpenShift, puede crear aplicaciones y administrar proyectos de OpenShift desde una terminal.
El binario oc ofrece las mismas capacidades que el binario kubectl, pero se amplía aún más para admitir de forma nativa las funciones de OpenShift Container Platform.

# odo - Developer-focused CLI for OpenShift
OpenShift Do (odo) es una herramienta CLI rápida, iterativa y sencilla para desarrolladores que escriben, compilan e implementan aplicaciones en OpenShift.
odo abstrae conceptos complejos de Kubernetes y OpenShift, lo que permite a los desarrolladores centrarse en lo que es más importante para ellos: el código.

 - https://mirror.openshift.com/pub/openshift-v4/clients/odo/latest/
 - https://github.com/openshift/odo

# Create Project
Cree un nuevo proyecto seleccionando Crear proyecto.
Al crear un proyecto, se le dejará en la página de descripción general del nuevo proyecto.
Si desea acceder a una lista de todos los proyectos que tiene disponibles, puede seleccionar "Inicio-> Proyectos" en el menú del lado izquierdo. Si no ve el menú, puede hacer clic en el botón del elemento del menú de hamburguesa en la esquina del nivel superior de la consola web.

# Logging in Via the Command Line
La consola web de OpenShift proporciona un método conveniente para interactuar rápidamente y ver el estado de las aplicaciones que ha implementado con OpenShift. No todo lo que quiera hacer se puede hacer a través de la consola web. Por lo tanto, también deberá estar familiarizado con el uso de la herramienta de línea de comandos de OpenShift oc.

Si estaba usando un clúster de OpenShift diferente y aún no tenía la herramienta de línea de comando oc, puede descargarla siguiendo los enlaces en la opción de menú Herramientas de línea de comando de la consola web.

![alt text](img/cli.png "cli")

Una vez que llegue a la lista de descargas, deberá descargar el archivo específico para su plataforma, extraer el binario oc e instalarlo.

Para iniciar sesión en el clúster de OpenShift utilizado para este curso, ejecute en la terminal:
```
$ oc login
Authentication required for https://openshift:6443 (openshift)
Username: developer
Password:
Login successful.

You have one project on this server: "myproject"

Using project "myproject".
```

Para su propio clúster, necesitaría conocer la URL para iniciar sesión en el clúster y pasarla como un argumento a oc login.
Una vez que haya iniciado sesión, puede verificar qué usuario ha iniciado sesión ejecutando:
```
oc whoami
```

You can verify which server you are logged into by running:
```
oc whoami --show-server
```

Puede enumerar todos los proyectos a los que tiene acceso actualmente ejecutando:
```
oc get projects
```


Incluso en el caso de que el clúster de OpenShift gestione la autenticación de usuario y se acepten las credenciales de usuario, puede optar por utilizar un token de acceso. Puede recuperar el comando para ejecutarlo ingresando manualmente la ruta "/oauth /token/request" con la URL del punto final de acceso al clúster.

Si ya ha iniciado sesión en la consola web, también puede recuperar los detalles del comando de inicio de sesión y el token de acceso accediendo a la opción de menú Copiar comando de inicio de sesión debajo de su nombre de inicio de sesión.

![alt text](img/02-login-access-token.png "login-access-token")